package com.thechalakas.jay.deviceconfiguration;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity","onCreate reached");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

        Log.i("MainActivity","Device Config Changed reached");

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            Log.i("MainActivity","LANDSCAPE reached");
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();


        }
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            Log.i("MainActivity","PORTRAIT reached");
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();

        }
    }
}
